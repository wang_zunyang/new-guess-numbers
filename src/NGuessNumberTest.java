
public class NGuessNumberTest {
	   @Test
       
       public void generateAnswerTest(){
               NGuessNumber c = new NGuessNumber();
               String result = c.generateAnswer();
              
               Assert.assertEquals(result.length(),4);
            
               int ad = Integer.valueOf(result);
               int a1,a2,a3,a4;
               a1 = ad/1000;
               a2 = ad%1000/100;
               a3 = ad%100/10;
               a4 = ad%10;
               Assert.assertEquals(a1!=a2,true);
               Assert.assertEquals(a1!=a3,true);
               Assert.assertEquals(a1!=a4,true);
               Assert.assertEquals(a2!=a3,true);
               Assert.assertEquals(a2!=a4,true);
               Assert.assertEquals(a3!=a4,true);
             
               Assert.assertEquals(a1>=0 && a1<=9,true);
               Assert.assertEquals(a2>=0 && a2<=9,true);
               Assert.assertEquals(a3>=0 && a3<=9,true);
               Assert.assertEquals(a4>=0 && a4<=9,true);
       }
	   @Test
      
       public void getPlayerInputTest(){
               NGuessNumber c = new NGuessNumber();
               String result = c.getPlayerInput();
            
               Assert.assertEquals(result.length(),4);
       }
	   @Test
     
       public void compareGuessAnswerTest(){
               NGuessNumber c = new NGuessNumber();
               String cga = c.compareGuessAnswer();
           
               Assert.assertEquals(cga.length(),4);
       }

}
